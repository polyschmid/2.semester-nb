/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package A16;

import java.util.LinkedList;
import java.util.List;

/**
 * Ein Objekt Autohaus, welches Autos verwalten kann.
 *
 * @author Hannes
 *
 */
public class Auto {

    private String Hersteller;
    private long Laufleistung;
    private double PreisInEUR;
    private String Farbe;
    private boolean Unfallwagen;
    private String Kraftstoff;
    private double LeistungInPS;
    private static List<Auto> Bestand = new LinkedList<Auto>();

    /**
     * Default Konstruktor (Wenn Leer)
     */
    public Auto() {

    }

    /**
     * Autohaus Konstruktor
     *
     * @param Hrs Hersteller String
     * @param Lauflei Laufleistung long
     * @param Preis PreisInEUR double
     * @param Farb Farbe String
     * @param Unfallw Unfallwagen boolean
     * @param Kraftst Kraftstoff String
     * @param PS LeistungInPS double
     */
    public Auto(String Hrs, long Lauflei, double Preis, String Farb, boolean Unfallw, String Kraftst, double PS) {
        this.Hersteller = Hrs;
        this.Laufleistung = Lauflei;
        this.PreisInEUR = Preis;
        this.Farbe = Farb;
        this.Unfallwagen = Unfallw;
        this.Kraftstoff = Kraftst;
        this.LeistungInPS = PS;
        Auto.Bestand.add(this);

    }

    /**
     * Getter Hersteller
     *
     * @return String Hersteller
     */
    public String getHersteller() {
        return Hersteller;
    }

    /**
     * Getter Laufleistung
     *
     * @return long Laufleistung
     */
    public long getLaufleistung() {
        return Laufleistung;
    }

    /**
     * Getter PreisInEUR
     *
     * @return double PreisInEUR
     */
    public double getPreisInEUR() {
        return PreisInEUR;
    }

    /**
     * Getter Farbe
     *
     * @return String Farbe
     */
    public String getFarbe() {
        return Farbe;
    }

    /**
     * is Unfallwagen
     *
     * @return boolean Unfallwagen
     */
    public boolean isUnfallwagen() {
        return Unfallwagen;
    }

    /**
     * Getter Kraftstoff
     *
     * @return String Kraftstoff
     */
    public String getKraftstoff() {
        return Kraftstoff;
    }

    /**
     * Getter LeistungInPS
     *
     * @return double LeistungInPS
     */
    public double getLeistungInPS() {
        return LeistungInPS;
    }

    /**
     * Liste der gepeicherten Autos
     *
     * @return LinkedList <Auto>
     */
    public static List<Auto> getBestand() {
        return Auto.Bestand;
    }

    /**
     * Gibt die Anzahl der gespeicherten Autos zur�ck.
     *
     * @return int Anzahl der Autos
     */
    public static int getAnzahl() {
        return Bestand.size();
    }

    /**
     * Sortiert die Liste des Bestands na dem Prei� absteigend
     *
     * @return LinkedList <Auto> sortiert nach Preis abs
     */
    public static void bubbleSort() {

        boolean unsorted = true;
        while (unsorted) {
            unsorted = false;
            for (int i = 0; i < Bestand.size() - 1; i++) {
                if (!(Bestand.get(i).getPreisInEUR() >= (Bestand.get(i + 1).getPreisInEUR()))) {
                    Auto dummy = Bestand.get(i);
                    Bestand.set(i, Bestand.get(i + 1));
                    Bestand.set(i + 1, dummy);
                    unsorted = true;
                }
            }
        }
    }

    /**
     * Gibt die Prozentanzahl der Autos Unfallwagen zur�ck.
     *
     * @return double Gibt den Prozentwert der Unfallwagen an
     */
    public static double anteil_unfallwagen() {
        double g = Bestand.size();
        double zaehler = 0;

        for (Auto i : Auto.Bestand) {
            if (i.Unfallwagen) {
                zaehler++;
            }
        }
        double prozentsatz = (zaehler / g) * 100;

        return Math.round(100.0 * prozentsatz) / 100.0;
    }

    /**
     * Gibt die Prozentanzahl der Kraftstofart an.
     *
     * @param e String Kraftstoffart
     * @return double Prozentwert
     */
    public static double anteil_kraftstoffart(String e) {
        double g = Bestand.size();
        double zaehler = 0;
        for (Auto i : Auto.Bestand) {
            if (e == i.getKraftstoff()) {
                zaehler++;
            }
        }

        double prozentsatz = (zaehler / g) * 100;
        return Math.round(100.0 * prozentsatz) / 100.0;
    }

    /**
     * Errechnet den Erl�s mit Nachl�sse
     *
     * @param autos Array , mit dem Objekt Auto.
     * @return String
     */
    public static double erloes_inkl_nachlass(double a, double b) {
        double zaehler = 0.0;

        for (Auto i : Auto.Bestand) {
            if (i.Unfallwagen) {
                double erlass = ((i.PreisInEUR / 100) * (b * 100));
                zaehler += i.PreisInEUR - erlass;

            } else {
                double erlass = ((i.PreisInEUR / 100) * (a * 100));
                zaehler += i.PreisInEUR - erlass;
            }
        }
        return Math.round(100.0 * zaehler) / 100.0;
    }

    /**
     * Errechnent die Prozentuale Steigerung mit 10% Erlass aller Wagen.
     *
     * @return double in Prozentuale Steigerung
     */
    public static double erloes_alle_frei_pr(double a, double b) {

        double ursprungswert = erloes_inkl_nachlass(a, b);
        double neuerwert = erloes_inkl_nachlass(a, a);
        double c = (neuerwert - ursprungswert) / ursprungswert * 100;

        return Math.round(100.0 * c) / 100.0;

    }

    /**
     * Errechnent Absolute Steigerung des Erl�ses in Euro
     *
     * @return double Absolute Steigerung
     */
    public static double erloes_alle_frei_eu(double a, double b) {
        double ursprungswert = erloes_inkl_nachlass(a, b);
        double erloesinproz = erloes_alle_frei_pr(a, a);
        double abssteigineu = ursprungswert * (erloesinproz / 100);
        return Math.round(100.0 * abssteigineu) / 100.0;

    }

    /**
     * toString Gibt das Auto als String aus.
     */
    public String toString() {
        String ausgabe = "";
        String a = "";
        if (this.Unfallwagen == false) {
            a += "unfallfrei\n";
        }
        ausgabe += "---\n"
                + "Hersteller: " + this.Hersteller + "\n"
                + "Preis: " + this.PreisInEUR + " EUR\n"
                + "Motor: " + this.LeistungInPS + " PS (" + this.Kraftstoff + ")" + "\n"
                + "KM-Stand: " + this.Laufleistung + " km\n"
                + "Farbe: " + this.Farbe + "\n"
                + a
                + "---";

        return ausgabe;
    }

    /**
     * Equals nach Java Konventionen
     *
     * @param a
     * @return
     */
    public boolean equals(Auto a) {
        return this.Hersteller == a.Hersteller
                && this.Laufleistung == a.Laufleistung
                && this.PreisInEUR == a.PreisInEUR
                && this.Farbe == a.Farbe
                && this.Unfallwagen == a.Unfallwagen
                && this.Kraftstoff == a.Kraftstoff
                && this.LeistungInPS == a.LeistungInPS;
    }

    /**
     * clone nach Java Konventionen
     */
    public Auto clone() {
        return new Auto(Hersteller, Laufleistung, PreisInEUR, Farbe, Unfallwagen, Kraftstoff, LeistungInPS);
    }
}
