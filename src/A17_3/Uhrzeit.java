package A17_3;

/**
 *
 * @author hannes
 */
public class Uhrzeit {
    private int stunde;
    private int minute;
    
    /**
     * Uhrzeit Konstruktor
     * @param s int Stunde
     * @param m  int Minute
     */
    public Uhrzeit(int s, int m){
        this.stunde = s;
        this.minute = m;
    }
        
    /**
     * toString , gibt die Zeit als String an.
     * @return String Zeit
     */
    public String toString(){
        return this.stunde +":"+this.minute+" Uhr";
    }
}
