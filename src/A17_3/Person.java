package A17_3;

/**
 *
 * @author hannes
 */
public class Person {
    private String name;
    private String nachname;
    
    /**
     * Konstruktor
     * @param vn String Name
     * @param nn String Nachname
     */
    public Person(String vn, String nn){
        this.name=vn;
        this.nachname=nn;
    }
    /**
     * Getter nach Konv.
     * @return  String  Name
     */
    public String getName(){
        return this.name;
    }
    /**
     * toString Gibt den Namen der Person als String zurück
     * @return String Name
     */    
    public String toString(){
        String ausgabe = this.name+" "+this.nachname;
        return ausgabe;
    }
}
