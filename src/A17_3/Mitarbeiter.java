package A17_3;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author hannes
 */
public class Mitarbeiter extends Person {
    private String email;
    protected List<Reservierung> buchung = new LinkedList<Reservierung>();
    
    
    /**
     * Konstruktor
     * @param vn String Name
     * @param nn String Nachname
     * @param em String E-Mailadresse
     */
    public Mitarbeiter(String vn,String nn, String em){
        super(vn, nn); //Durch die Vererbung kann man die werte von der Superklasse abfragen
        this.email = em;
    }
    /**
     * toString Gibt den Mitarbeiter als String zurück
     * @return String Mitarbeiter
     */
    public String toString(){
        return super.toString() + " ("+this.email+")"; // Ruft auch Methoden auf aus der Superklasse
    }

    /**
     * Reserviert einen Raum für eine Person.
     * @param r Raum
     * @param a von - Uhrzeit(Anfang)
     * @param e bis - Uhrzeit(Ende)
     * @param b String Bemerkung
     */
    public void reserviere(Raum r, Uhrzeit a, Uhrzeit e, String b){
       Reservierung re = new Reservierung(a,e);
       re.setRaum(r);     
       re.setVon(this);
       re.setBemerkung(b);
       
        
    }
}
