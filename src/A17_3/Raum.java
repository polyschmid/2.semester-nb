package A17_3;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author hannes
 */
public class Raum {
    private int geb;
    private int etage;
    private int raum;
    protected List<Reservierung> reservierungen = new LinkedList<Reservierung>();

   
    
    
    /**
     * Konstruktor
     * @param g int Gebäude
     * @param e int Etage
     * @param r int Raum
     */
    public Raum (int g, int e, int r){
        this.geb = g;
        this.etage = e;
        this.raum = r;
    }

    /**
     * ToString
     * @return Gibt den Raum inclusive Reservierung an.
     */
    public String toString(){
        String ausgabe ="";
        
        for(Reservierung r: this.reservierungen){
            ausgabe += r.toString();
        }
        
        
        return "Raum "+ this.geb + "-" + this.etage+ "." + this.raum +"\n"+ausgabe;
    }

    /**
     * Fügt eine Reservierung durch.
     * @param r Reservierung
     */
    public void addReservierung(Reservierung r){
        this.reservierungen.add(r);
        
    }
}
