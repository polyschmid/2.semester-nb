
package A17_3;

/**
 *
 * @author hannes
 */
public class Reservierung {
    private String bemerkung;
    private Uhrzeit beginn;
    private Uhrzeit ende;
    protected Mitarbeiter von;
    protected Raum raum;
    
    /**
     * Konstruktor
     * @param b Beginn Uhrzeit
     * @param e Ende Uhrzeit
     */
    public Reservierung (Uhrzeit b, Uhrzeit e){
        this.beginn = b;
        this.ende = e;
    }

    /**
     * Setter Bemerkung String
     * @param String bemerkung 
     */
    public void setBemerkung(String bemerkung) {
        this.bemerkung = bemerkung;
    }


    /**
     * Setter von Mitarbeiter
     * @param von Mitarbeiter
     */
    public void setVon(Mitarbeiter von) {
        this.von = von;
    }
    
    /**
     * Setter von Raum
     * @param raum Raum
     */
    public void setRaum(Raum raum){
         this.raum = raum;
         raum.addReservierung(this);
    }
    
    /**
     * ToString 
     * @return String gibt die Reservierung als String zurück.
     */
    public String toString(){
        
        if(raum.reservierungen.size() >= 2){
            return "gebucht von "+ von +" von "+beginn+" bis "+ende+" für "+bemerkung+"\n";
        } else{
            return "gebucht von "+ von +" von "+beginn+" bis "+ende+" für "+bemerkung;
        }
    }
}
