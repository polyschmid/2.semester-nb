package A17_4;

/**
 *
 * @author Hannes
 */
public class Student implements Mitarbeiter {
    private String name;
    private String mail;
    private int matrNr;
    private Raum raum;
    
    
    /**
     * Konstrukter des Studenten
     * @param n String Name
     * @param m String Mail
     * @param nr Integer Matrikelnummer
     */
    public Student (String n, String m, int nr){
        this.name = n;
        this.mail = m;
        this.matrNr = nr;
    }
    
    /**
     * Getter
     * @return String Name
     */
    
    public String getName() {
        return this.name;
    }
    
    /**
     * Getter
     * @return Raum Raum 
     */
    public Raum getRaum() {
        return this.raum;
    }
    
    /**
     * Setter
     * @param r Raum 
     */
    
    public void setRaum(Raum r) {
        this.raum = r;
    }
    /**
     * toString
     * @return String
     */
    
    public String toString(){
        return this.name+" ("+this.mail+")";
    }
    
    public int compareTo(Mitarbeiter m) {
        return this.name.compareTo(m.getName());
    }
}
