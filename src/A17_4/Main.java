package A17_4;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Hannes
 */
public class Main {

    public static void main(String[] args) {
        Telefonanschluss t = new Telefonanschluss(451, 300, 5549);
        
        
        
        Mitarbeiter m0 = new Angestellter("Prof. Dr.", "Hans Peter", "musterfrau@fhl.de");
        Mitarbeiter s0 = new Student("Grummel Hans", "max.mustermann@stud.fhl.de", 456442);
        Mitarbeiter m1 = new Angestellter("Prof. Dr.", "Musterfrau", "musterfrau@fhl.de");
        Mitarbeiter s1 = new Student("Max Mustermann", "max.mustermann@stud.fhl.de", 123456);
        Raum r = new Raum(18, 0, 1);
        r.weiseZu(m0);
        r.weiseZu(s0);
        Raum r1 = new Raum(19, 1, 3);
        r1.weiseZu(m1);
        r1.weiseZu(s1);
        
        
        Telefonanschluss t1 = new Telefonanschluss(451, 300, 4321);
        Telefonanschluss t2 = new Telefonanschluss(451, 300, 1234);
        
        r.weiseZu(t1);
        r1.weiseZu(t2);
        
        
        List<Mitarbeiter> mas = new ArrayList<Mitarbeiter>();
        mas.add(m0);
        mas.add(s0);
        mas.add(m1);
        mas.add(s1);

        System.out.print(telefonbuch(mas));
    }
    
    /**
     * Gibt die Mitarbeiter alphabetisch aus.
     * @param mas List
     * @return String Sortiertes Telefonbuch
     */
    public static String telefonbuch(List<Mitarbeiter> mas){
        
        /*
        Da die eingangs Liste kin prim Datentyp ist stellen wir eine
        Prim Liste zum sortieren!
        */
        List<String> sortmas = new ArrayList<String>();
        
        String ausgabe = "";
        
        
        for(Mitarbeiter m : mas){
            ausgabe += m.toString()+"\n";
            Raum r = m.getRaum();
            for (Telefonanschluss tel : r.getAnschluesse()){
                ausgabe += "- " + tel + " " + r +")\n";
            }
            /*
            Der String wird der Liste hinzugefügt.
            */
            sortmas.add(ausgabe);
            /*
            Der String wird wieder auf NULL gesetzt damit
            die Schleife von vorn beginnen kann und den String nicht einfach
            verlängert.
            */
            ausgabe = "";
        }
        /*
        Die Prim Liste wird sortiert.
        */
        sortmas.sort(null);
        /*
        Nun wird der String wieder zusammengefügt.
        */
        for (String s : sortmas){
            ausgabe += s; 
        }
        return ausgabe; //ausgabe.substring(0, ausgabe.length()-1);
    }

    
}
