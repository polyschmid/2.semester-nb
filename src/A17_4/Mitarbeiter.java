package A17_4;

/**
 *
 * @author Hannes
 */
public interface Mitarbeiter {
    /**
     * Getter
     * @return String  Name 
     */
    public String getName();
    
    /**
     * Getter
     * @return  Raum
     */
    public Raum getRaum();
    
    /**
     * Setter
     * @param r Raum setzt den Raum
     */
    public void setRaum(Raum r);
    
    /**
     * Getter
     * @return gibt den Mitarbeiter als String zurück
     */
    public String toString();


}
