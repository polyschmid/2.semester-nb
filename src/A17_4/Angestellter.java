package A17_4;

/**
 *
 * @author Hannes
 */
public class Angestellter implements Mitarbeiter {

    private String name;
    private String mail;
    private String titel;
    private Raum raum;

    /**
     * Konstruktor des Angestellten
     *
     * @param n String Name
     * @param m String Mail
     * @param t String Titel
     */
    public Angestellter(String t, String n, String m) {
        this.name = n;
        this.mail = m;
        this.titel = t;
    }

    /**
     * Getter
     *
     * @return String Name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter
     *
     * @return Raum Raum
     */
    public Raum getRaum() {
        return this.raum;
    }

    /**
     * Setter
     *
     * @param r Raum
     */
    public void setRaum(Raum r) {
        this.raum = r;
    }

    /**
     * toString
     *
     * @return String
     */
    public String toString() {
        return this.titel + " " + this.name + " (" + this.mail + ")";
    }
    /**
     * Vergleicht 
     * @param m
     * @return 
     */
    public int compareTo(Mitarbeiter m) {
        return this.name.compareTo(m.getName());
    }

}
