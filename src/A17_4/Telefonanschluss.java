package A17_4;

/**
 *
 * @author Hannes
 */
public class Telefonanschluss {
    private int vorwahl;
    private int durchwahl;
    private int hauptwahl;
    private Raum raum;
    
    /**
     * Konstruktor
     * @param v int Vorwahl
     * @param d int Durchwahl
     * @param h int Hauptwahl
     */
    public Telefonanschluss(int v, int d, int h){
        this.vorwahl = v;
        this.durchwahl = d;
        this.hauptwahl = h;
        
    }
    
    /**
     * Setter Setzt den Raum auf den Telefonanschluss
     * @param raum 
     */
    public void setRaum(Raum raum){
        this.raum = raum;
    }
    
    /**
     * toString Gibt die Telefonummer wieder
     * @return String Gibt den Telnummer aus.
     */
    public String toString(){
        return this.vorwahl+"-"+this.durchwahl+" "+this.hauptwahl;
    }
}
