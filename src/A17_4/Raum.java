package A17_4;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Hannes
 */
public class Raum {
    private int geb;
    private int etage;
    private int raum;
    private List<Mitarbeiter> mitarbeiter = new LinkedList<Mitarbeiter>();
    private List<Telefonanschluss> anschluesse = new LinkedList<Telefonanschluss>();
    
    /**
     * Konstruktor
     * @param g int Gebäude
     * @param e int Etage
     * @param r int Raum
     */
    public Raum(int g, int e, int r){
        this.geb = g;
        this.etage = e;
        this.raum = r;
    }
    
    /**
     * toString
     * @return String gibt den Raum aus-
     */
    public String toString(){
        return "Raum "+this.geb+"-"+this.etage+"."+this.raum;
    }
    
    /**
     * Fügt ein mitarbeiter einer Liste hinzu.
     * @param m Mitarbeiter
     * @return boolean 
     */
    public boolean weiseZu(Mitarbeiter m){
        
        boolean ausgabe = false;
        if(mitarbeiter.contains(m)){
            ausgabe = false;
        } else {
            ausgabe = true;
            mitarbeiter.add(m); //Mitarbeiter wird der Liste hinzugefügt.
            m.setRaum(this);    //Der Raum wird auf den Mitarbeiter gesetzt!!!
        }
        return ausgabe;
    }
    
    /**
     * Fügt ein Telefonanschluss einer Liste hinzu.
     * @param t Telefonanschluss
     * @return boolean
     */
    public boolean weiseZu(Telefonanschluss t){
        boolean ausgabe = false;
        
        if(anschluesse.contains(t)){
            ausgabe = false;
        }else{
            ausgabe = true;
            anschluesse.add(t); //
            t.setRaum(this);    // Hier wird auf die Tel der Raum gesetzt.!!
        }
        
        return ausgabe;
    }
    
    /**
     * Getter
     * @return List<Mitarbeiter> 
     */
    public List<Mitarbeiter> getMitarbeiter(){
        return this.mitarbeiter;
        
    }
    
    /**
     * Getter
     * @return  List<Telefonanschluss>
     */
    public List<Telefonanschluss> getAnschluesse(){
        return this.anschluesse;
    }
    
}
