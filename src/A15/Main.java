package A15;

public class Main {
	public static void main(String[] args) {
//		Auto a1 = new Auto("Ford", 125000, 7999.99, "silber metallic", false, "Diesel", 101.0);
//		Auto a2 = new Auto("Mercedes", 63000, 22999.99, "blue silver", true, "Super", 137.0);
//		Auto a3 = new Auto("Daihatsu", 12000, 3099.99, "green dynamite", true, "Benzin", 75.0);
//		System.out.println(a1);
//		System.out.println(a2);
//		System.out.println(a3);
		
		Auto[] autos = {new Auto("Ford", 125000, 7999.99, "silber metallic", false, "Diesel", 101.0),
						new Auto("Mercedes", 63000, 22999.99, "blue silver", true, "Super", 137.0),
						new Auto("Daihatsu", 12000, 3099.99, "green dynamite", false, "Benzin", 75.0)};
		/**
		 * Sortiert das AutoArray absteigend
		 */
		bubbleSort(autos);
		for(Auto a : autos) {
			System.out.println(a);
		}
		
		///////////////
		
		Auto[] autobestand = {
				new Auto("Ford", 125000, 7999.99, "silber metallic", false, "Diesel", 101.0),
				new Auto("Mercedes", 63000, 22999.99, "blue silver", true, "Super", 137.0),
				new Auto("Daihatsu", 12000, 3099.99, "green dynamite", false, "Benzin", 75.0),
				new Auto("Ford", 1700, 17999.99, "silber metallic", false, "Diesel", 101.0),
				new Auto("Mercedes", 63000, 22999.99, "blue silver", true, "Elektro", 37.0),
				new Auto("Daihatsu", 12000, 3099.99, "green dynamite", true, "Benzin", 75.0),
				new Auto("Ford", 12500, 12999.99, "silber metallic", false, "Super", 121.0),
				new Auto("Mercedes", 6300, 32999.99, "blue silver", false, "Super", 137.0),
				new Auto("Daihatsu", 12000, 3099.99, "green dynamite", true, "Benzin", 75.0),
				new Auto("Ford", 1700, 17999.99, "silber metallic", false, "Diesel", 101.0),
				new Auto("Mercedes", 63000, 22999.99, "blue silver", true, "Elektro", 37.0),
				new Auto("Daihatsu", 12000, 3099.99, "green dynamite", true, "Benzin", 75.0)
		};
		


		
		System.out.println("Erl�s ohne Nachl�sse: " + ErloesOhneNachleassen(autobestand) + " EUR");
		System.out.println("Anteil an Unfallwagen: " + AntUnfallwagen(autobestand) + " %");
		System.out.println("Anteil an Dieselwagen: " + AntDieselwagen(autobestand) + " %");
		System.out.println("Anteil an Elektrowagen: " + AntElektrowagen(autobestand) + " %");
		System.out.println("Erl�s mit Nachl�ssen: " + ErloesMitNachleassen(autobestand) + " EUR");					
	}
	/**
	 * Errechnet den Erl�s ohne Nachl�sse
	 * @param autos Array , mit dem Objekt Auto.
	 * @return String
	 */
	public static double ErloesOhneNachleassen(Auto[] autos) {
		
		double zaehler = 0.0;
		for(Auto i : autos){
			zaehler += i.PreisInEUR;
		}
		return Math.round(100.0*zaehler)/100.0;
	}
	/**
	 * Gibt die Prozentanzahl der Autos Unfallwagen zur�ck.
	 * @param autos Array , mit dem Objekt Auto.
	 * @return String, gibt die Unfallwagen in Prozent zur�ck
	 *
	 */
	public static double AntUnfallwagen(Auto[] autos) {
		double g = autos.length;
		double zaehler = 0;
		for(Auto i : autos){
			if(i.Unfallwagen==true){
                            zaehler++;
                        }
		}
		double prozentsatz = (zaehler/g)*100;
		
		return Math.round(100.0*prozentsatz)/100.0;
	}
	/**
	 * Gibt die Prozentanzahl der Autos als Dieselwagen zur�ck.
	 * @param autos Array , mit dem Objekt Auto.
	 * @return String, gibt die Dieselwagen in Prozent zur�ck
	 */
	public static double AntDieselwagen(Auto[] autos) {
		double g = autos.length;
		double zaehler = 0;
		for(Auto i : autos){
			if(i.Kraftstoff == "Diesel") zaehler++;
		}
		double prozentsatz = (zaehler/g)*100;
		
		return Math.round(100.0*prozentsatz)/100.0;
	}
	/**
	 * Gibt die Prozentanzahl der Autos als Elektrowagen zur�ck.
	 * @param autos Array , mit dem Objekt Auto.
	 * @return String, gibt die Elektrowagen in Prozent zur�ck
	 */
	public static double AntElektrowagen(Auto[] autos) {
		double g = autos.length;
		double zaehler = 0;
		for(Auto i : autos){
			if(i.Kraftstoff == "Elektro") zaehler++;
		}
		double prozentsatz = (zaehler/g)*100;
		
		return Math.round(100.0*prozentsatz)/100.0;
	}
	
	/**
	 * Errechnet den Erl�s mit Nachl�sse
	 * @param autos Array , mit dem Objekt Auto.
	 * @return String
	 */
	public static double ErloesMitNachleassen(Auto[] autos) {
            double zaehler = 0.0;

            for(Auto i : autos){
                    if(i.Unfallwagen==true){
                            double erlass = ((i.PreisInEUR/100)*25);
                            zaehler += i.PreisInEUR  - erlass;

                    }else{
                            double erlass = ((i.PreisInEUR/100)*10);
                            zaehler += i.PreisInEUR  - erlass;
                    }
            }
            return Math.round(100.0*zaehler)/100.0;
	}
	/**
	 * Sortiert Das AutoArray na dem Prei� absteigend
	 * @param xs
	 */
	public static void bubbleSort(Auto[] xs) {
		
            boolean unsorted=true;
            while (unsorted) {
                    unsorted = false;
                    for (int i=0; i < xs.length-1; i++) {
                            if (!(xs[i].PreisInEUR >= xs[i+1].PreisInEUR)) {
                                    Auto dummy = xs[i];
                                    xs[i] = xs[i+1];
                                    xs[i+1] = dummy;
                                    unsorted = true;
                            }
                    }
            }
	}
}