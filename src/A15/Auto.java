package A15;

/**
 * Ein Objekt Autohaus, welches Autos verwalten kann.
 * @author Hannes
 *
 */
public class Auto {
	 public String Hersteller;
	 public long Laufleistung;
	 public double PreisInEUR;
	 public String Farbe;
	 public boolean Unfallwagen;
	 public String Kraftstoff;
	 public double LeistungInPS;	
	

	/**
	 * Autohaus Konstruktor
	 * @param Hrs Hersteller String
	 * @param Lauflei Laufleistung long
	 * @param Preis PreisInEUR double
	 * @param Farb Farbe String
	 * @param Unfallw Unfallwagen boolean
	 * @param Kraftst Kraftstoff String
	 * @param PS LeistungInPS double
	 */
	public Auto(String Hrs, long Lauflei, double Preis, String Farb, boolean Unfallw, String Kraftst, double PS){
		this.Hersteller = Hrs;
		this.Laufleistung = Lauflei;
		this.PreisInEUR = Preis;
		this.Farbe = Farb;
		this.Unfallwagen = Unfallw;
		this.Kraftstoff = Kraftst;
		this.LeistungInPS = PS;
	}
	/**
	 * toString
	 * Gibt das Auto als String aus.
	 */
	public String toString(){
		String ausgabe = "";
		String a="";
		if(this.Unfallwagen == false) a+="unfallfrei\n";
		ausgabe+= 	"---\n"+
					"Hersteller: " + this.Hersteller + "\n" + 
					"Preis: " + this.PreisInEUR + " EUR\n" + 
					"Motor: " + this.LeistungInPS +" PS ("+this.Kraftstoff+")"+ "\n" + 
					"KM-Stand: " +  this.Laufleistung + " km\n" + 
					"Farbe: " + this.Farbe + "\n" +
					a+
					"---";
		
		
		return	ausgabe;
	}
	/**
	 * Equals nach Java Konventionen
	 * @param a
	 * @return
	 */
	public boolean equals (Auto a){
		return	this.Hersteller == a.Hersteller &&
				this.Laufleistung == a.Laufleistung &&
				this.PreisInEUR == a.PreisInEUR &&
				this.Farbe == a.Farbe &&
				this.Unfallwagen == a.Unfallwagen &&
				this.Kraftstoff == a.Kraftstoff &&
				this.LeistungInPS == a.LeistungInPS;
	}
	/**
	 * clone nach Java Konventionen
	 */
	public Auto clone(){
		return new Auto(Hersteller, Laufleistung, PreisInEUR, Farbe, Unfallwagen, Kraftstoff, LeistungInPS);
	}
}
	