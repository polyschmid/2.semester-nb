package A17;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author hannes
 */
public class Bestand {

    private List<Auto> autos ;

    /**
     * Konstruktor
     */
    public Bestand() {
        autos = new LinkedList<Auto>();
    }

    /**
     * Diese Methode prüft, ob ein Auto bereits im Bestand ist. Liefert true
     * wenn ja, andernfalls false.
     *
     * @param a Auto
     * @return boolean true = Vorhanden, false = nicht Vorhanden
     */
    public boolean istVorhanden(Auto a) {
        boolean ausgabe = false;
        if (!autos.contains(a)) {
            ausgabe = true;
        }
        return ausgabe;
    }

    /**
     * Diese Methode fügt ein Auto-Objekt zum Bestand hinzu. Der Parameter
     * referenziert das Auto-Objekt, das dem Bestand hinzugefügt werden soll.
     * Die Methode liefert false, wenn Auto-Objekt schon im Bestand vorhanden
     * ist, ansonsten true.
     *
     * @param a Auto
     * @return boolean
     */
    public boolean aufnehmen(Auto a) {
        boolean ausgabe = false;
        if (!autos.contains(a)) {
            autos.add(a);
            ausgabe = true;
        }
        return ausgabe;
    }
    /**
     * Entnimmt ein Auto und gibt an ob es funktioniert hat oder nicht.
     * @param a Auto
     * @return boolean Gibt true = hinzugefügt oder false = ist vorhanden zurück. 
     */
    public boolean entnehmen(Auto a){
        boolean ausgabe = false;
        if(autos.contains(a)){
           autos.remove(a);
           ausgabe = true;
        }
        return ausgabe;
    }

    /**
     * Diese Methode liefert alle Auto-Objekte des Bestands als Array.
     *
     * @return Auto[] autos
     */
    public Auto[] getAutosAlsArray() {
        Auto[] auto = autos.toArray(new Auto[autos.size()]);
        //Auto[] auto = new Auto[autos.size()];
        //autos.toArray(auto);
        return auto;
    }
    /**
     * Sortiert das Autos Array nach Preis absteigend.
     * @return Array gibt ein Sortiertes Autoarray zurück.
     */
    public Auto[] sortiertNachPreis() {
        Auto[] xs = getAutosAlsArray();
        boolean unsorted = true;
        while (unsorted) {
            unsorted = false;
            for (int i = 0; i < xs.length - 1; i++) {
                if (!(xs[i].getPreis() >= xs[i + 1].getPreis())) {
                    Auto dummy = xs[i];
                    xs[i] = xs[i + 1];
                    xs[i + 1] = dummy;
                    unsorted = true;
                }
            }
        }
        return xs;
    }   
}
