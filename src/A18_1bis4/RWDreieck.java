package A18_1bis4;

/**
 *
 * @author hannes
 */
public class RWDreieck extends FigMZLA {

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     * @param a int Maße "a" des Objekt
     * @param b int Maße "b" des Objekt
     */
    public RWDreieck(int x, int y, int z, int a, int b) {
        super(x, y, z, a, b);

    }

    /**
     * Berechnet die Fläche des Dreiecks
     *
     * @return double in FE
     */
    public double berechneFlaeche() {
        return Math.abs((super.A * super.B) / 2);
    }

}
