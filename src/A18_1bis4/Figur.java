package A18_1bis4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author hannes
 */
public class Figur {

    protected int X;
    protected int Y;
    protected int Z;
    private static List<Figur> figuren = new ArrayList<Figur>();

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     */
    public Figur(int x, int y, int z) {
        this.X = x;
        this.Y = y;
        this.Z = z;
        figuren.add(this);
    }

    /**
     * Gibt die Anzal der Figuren Zurück
     *
     * @return int Anzal der Figuren
     */
    public static final int anzFiguren() {
        return figuren.size();
    }

    /**
     * Getter für die Figuren
     *
     * @return List<Figur> Liste der Figuren
     */
    public static final List<Figur> getAlleFiguren() {
        return figuren;
    }

    /**
     * Sortiert die Figuren in der z-Ebene
     *
     * @return List<Figur> sortiert in der Z-Ebene
     */
    public static List<Figur> getZsortierteFiguren() {
        Figur[] xs = figuren.toArray(new Figur[figuren.size()]);
        boolean unsorted = true;
        while (unsorted) {
            unsorted = false;
            for (int i = 0; i < xs.length - 1; i++) {
                if (!(xs[i].Z <= xs[i + 1].Z)) {
                    Figur dummy = xs[i];
                    xs[i] = xs[i + 1];
                    xs[i + 1] = dummy;
                    unsorted = true;
                }
            }
        }
        List<Figur> t = new ArrayList<Figur>(Arrays.asList(xs));
        return t;
    }

    /**
     * Filtert Obejkte von einer Liste heraus.
     *
     * @param a int Anfangswert des Filters
     * @param b int Endwert des Filters
     * @param c List<Figur> Liste die gefiltert werden soll
     * @return List<Figur> Gibt eine gefilterte Liste zurück
     */
    public static List<Figur> filterZ(int a, int b, List<Figur> c) {
        List<Figur> FigGefilt = new ArrayList<Figur>();
        for (Figur fig : c) {
            if ((fig.Z >= a) & (fig.Z <= b)) {
                FigGefilt.add(fig);
            }
        }
        return FigGefilt;
    }

    /**
     * Berechnent die gesamt Fläche von Objekten in einer Liste
     *
     * @param c List<Figur> Liste dessen Fläche berechnet werden soll
     * @return double Gibt die gesamt Fläche zurück.
     */
    public static double gesamtFlaeche(List<Figur> c) {
        double erg = 0;
        for (Figur fig : c) {
            erg += ((FigMZLA) fig).berechneFlaeche(); // Klammerregel und Casten!
        }
        return erg;
    }

    /**
     * Berechnent die durchschnitts Fläche von Objekten in einer Liste
     *
     * @param c List<Figur> Liste dessen Fläche berechnet werden soll
     * @return double Gibt die gesamt Fläche zurück.
     */
    public static double gesamtDurchschFlaeche(List<Figur> c) {
        double erg = 0;
        for (Figur fig : c) {
            erg += ((FigMZLA) fig).berechneFlaeche(); // Klammerregel und Casten!
        }
        erg = erg / c.size();
        return erg;
    }

}
