package A18_1bis4;

/**
 *
 * @author hannes
 */
public class Rechteck extends FigMZLA {

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     * @param a int Maße "a" des Objekt
     * @param b int Maße "b" des Objekt
     */
    public Rechteck(int x, int y, int z, int a, int b) {
        super(x, y, z, a, b);
    }

    /**
     * Berechnet die Fläche des Rechtecks.
     *
     * @return double Fläche des Objektes
     */
    public double berechneFlaeche() {
        return Math.abs(super.A * super.B);
    }

}
