package A18_1bis4;

/**
 *
 * @author hannes
 */
public class Ellipse extends FigMZLA {

    /**
     * Konstruktor
     *
     * @param x int Punkt im Raum
     * @param y int Punkt im Raum
     * @param z int Punkt im Raum
     * @param a int Maße "a" des Objekt
     * @param b int Maße "b" des Objekt
     */
    public Ellipse(int x, int y, int z, int a, int b) {

        super(x, y, z, a, b);
    }

    /**
     * Berechnet die Fläche des Objekts
     *
     * @return double Fläche des Objekts
     */
    public double berechneFlaeche() {
        return Math.abs(Math.PI * super.A * super.B);
    }

}
