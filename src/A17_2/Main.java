package A17_2;
/**
 * 
 * @author hannes
 */
public class Main {

    public static void main(String[] args) {

        Bestand bestand = new Bestand();
        Auto a = new Auto("Ford", 125000, 7999.99, "silber metallic", false, "Diesel", 101.0);
            a.zumBestandHinzu(bestand);
            
            a = new Auto("Mercedes", 63000, 22999.99, "blue silver", true, "Super", 137.0);
            a.zumBestandHinzu(bestand);
            
            a = new Auto("Daihatsu", 12000, 3099.99, "green dynamite", false, "Benzin", 75.0);
            a.zumBestandHinzu(bestand);
            
            a = new Auto("Ford", 1700, 17999.99, "silber metallic", false, "Diesel", 101.0);
            a.zumBestandHinzu(bestand);
            
            a = new Auto("Mercedes", 63000, 22999.99, "blue silver", true, "Elektro", 37.0);
            a.zumBestandHinzu(bestand);
            
            a = new Auto("Daihatsu", 12000, 3099.99, "green dynamite", true, "Benzin", 75.0);
            a.zumBestandHinzu(bestand);
            
            a = new Auto("Ford", 12500, 12999.99, "silber metallic", false, "Super", 121.0);
            a.zumBestandHinzu(bestand);
            
            a = new Auto("Mercedes", 6300, 32999.99, "blue silver", false, "Super", 137.0);
            a.zumBestandHinzu(bestand);
            
            a = new Auto("Daihatsu", 12000, 3099.99, "green dynamite", true, "Benzin", 75.0);
            a.zumBestandHinzu(bestand);
            
            a = new Auto("Ford", 1700, 17999.99, "silber metallic", false, "Diesel", 101.0);
            a.zumBestandHinzu(bestand);
            
            a = new Auto("Mercedes", 63000, 22999.99, "blue silver", true, "Elektro", 37.0);
            a.zumBestandHinzu(bestand);
            
            a = new Auto("DaihatsuT", 12000, 3099.99, "green dynamite", true, "Benzin", 75.0);
            a.zumBestandHinzu(bestand);
            //a.ausDemBestand();
            
            
            Bestand bestand2 = new Bestand();
            a = new Auto("Forfgdztd", 125000, 7999.99, "silber metallic", false, "Diesel", 101.0);
            a.zumBestandHinzu(bestand2);
            
            
        for (Auto auto : bestand.sortiertNachPreis()) {
            System.out.println(auto);
        }
        
        for (Auto auto : bestand2.sortiertNachPreis()) {
            System.out.println(auto);
        }
        
        System.out.println("Prozentuale Steigerung des Erlöses: " + bestand.erloes_alle_frei_pr(0.1,0.25) + " %");
        System.out.println("Absolute Steigerung des Erlöses: " + bestand.erloes_alle_frei_eu(0.1,0.25) + " EUR");
    }
}
