package A17_2;
/**
 * Ein Objekt Autohaus, welches Autos verwalten kann.
 *
 * @author Hannes
 *
 */
public class Auto {
    private String hersteller;
    private long laufleistung;
    private double preis;
    private String farbe;
    private boolean unfallwagen;
    private String kraftstoff;
    private double psLeistung;
    private static Bestand bestand; // hier wird der bestand übergeben
   

  /**
   * Konstruktor
   * @param Hersteller String
   * @param Laufleistung long
   * @param Preis double
   * @param Farbe String
   * @param Unfallwagen boolean
   * @param Kraftstoff String
   * @param PsLeistung double
   */
    public Auto(String Hersteller, long Laufleistung, double Preis, String Farbe, boolean Unfallwagen, String Kraftstoff, double PsLeistung) {
        this.hersteller = Hersteller;
        this.laufleistung = Laufleistung;
        this.preis = Preis;
        this.farbe = Farbe;
        this.unfallwagen = Unfallwagen;
        this.kraftstoff = Kraftstoff;
        this.psLeistung = PsLeistung;
        
    }
    /**
     * Gibt den Preis an.
     * @return  double Preis des Autos
     */
    public double getPreis(){
        return preis;
    }
    /**
     * Gibt den Unfallwagen zurück.
     * @return boolean
     */
    public boolean isUnfallwagen(){
        return unfallwagen;
    }
    /**
     * boolean Diese Methode fügt ein Auto-Objekt in einen Bestand ein. 
     * @param b Bestand
     * @return boolean gibt an ob das hinzufügen gelungen ist.
     */
    public boolean zumBestandHinzu(Bestand b){
        boolean ausgabe = false;
        if(b!=null){
           
           ausgabe = b.aufnehmen(this);
           if(ausgabe){
               this.bestand = b;
           }
        }
        return ausgabe;
    }
    
    /**
     * Entfernt ein Auto aus dem Bestand.
     * @return boolean false =entfernen nicht erfolgreich, true = entfernen erfolgreich
     */
    public boolean ausDemBestand(){
        boolean ausgabe = false;
        if(bestand!=null){
            ausgabe = bestand.entnehmen(this);
            bestand = null;
        }
        return ausgabe;
    }
    /**
     * toString Gibt das Auto als String aus.
     */
    public String toString() {
        String ausgabe = "";
        String a = "";
        if (this.unfallwagen == false) {
            a += "unfallfrei\n";
        }
        ausgabe += "---\n"
                + "Hersteller: " + this.hersteller + "\n"
                + "Preis: " + this.preis + " EUR\n"
                + "Motor: " + this.psLeistung + " PS (" + this.kraftstoff + ")" + "\n"
                + "KM-Stand: " + this.laufleistung + " km\n"
                + "Farbe: " + this.farbe + "\n"
                + a
                + "---";
        return ausgabe;
    }
}