package A17_2;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author hannes
 */
public class Bestand {
    /**
     * Liste der Autos (Bestand)
     */
    private List<Auto> autos = new LinkedList<Auto>();

    /**
     * Default Konstruktor 
     */
    public Bestand() {
        
    }

    /**
     * Diese Methode prüft, ob ein Auto bereits im Bestand ist. Liefert true
     * wenn ja, andernfalls false.
     *
     * @param a Auto
     * @return boolean true = Vorhanden, false = nicht Vorhanden
     */
    public boolean istVorhanden(Auto a) {
        boolean ausgabe = false;
        if (autos.contains(a)) {
            ausgabe = true;
        }
        return ausgabe;
    }

    /**
     * Diese Methode fügt ein Auto-Objekt zum Bestand hinzu. Der Parameter
     * referenziert das Auto-Objekt, das dem Bestand hinzugefügt werden soll.
     * Die Methode liefert false, wenn Auto-Objekt schon im Bestand vorhanden
     * ist, ansonsten true.
     *
     * @param a Auto
     * @return boolean
     */
    public boolean aufnehmen(Auto a) {
        boolean ausgabe = false;
        if (!istVorhanden(a)) {
            autos.add(a);
            ausgabe = true;
        }
        return ausgabe;
    }
    /**
     * Entnimmt ein Auto und gibt an ob es funktioniert hat oder nicht.
     * @param a Auto
     * @return boolean Gibt true = hinzugefügt oder false = ist vorhanden zurück. 
     */
    public boolean entnehmen(Auto a){
        boolean ausgabe = false;
        if(autos.contains(a)){
           autos.remove(a);
           ausgabe = true;
        }
        return ausgabe;
    }

    /**
     * Diese Methode liefert alle Auto-Objekte des Bestands als Array.
     *
     * @return Auto[] autos
     */
    public Auto[] getAutosAlsArray() {
        //Auto[] auto = Bestand.autos.toArray(new Auto[Bestand.autos.size()]);
        Auto[] auto = new Auto[autos.size()];
        autos.toArray(auto);
        return auto;
    }
    /**
     * Sortiert das Autos Array nach Preis absteigend.
     * @return Array gibt ein Sortiertes Autoarray zurück.
     */
    public Auto[] sortiertNachPreis() {
        Auto[] xs = getAutosAlsArray();
        boolean unsorted = true;
        while (unsorted) {
            unsorted = false;
            for (int i = 0; i < xs.length - 1; i++) {
                if (!(xs[i].getPreis() >= xs[i + 1].getPreis())) {
                    Auto dummy = xs[i];
                    xs[i] = xs[i + 1];
                    xs[i + 1] = dummy;
                    unsorted = true;
                }
            }
        }
        return xs;
    }
    /**
     * Gibt den Erlös inklusive Nachlass zurück, je nach Unfallwagen "b" oder nicht "a".
     * @param a double kein Unfallwagen
     * @param b double Unfallwagen
     * @return double Erlös
     */
    public double erloes_inkl_nachlass(double a, double b) {
        double zaehler = 0.0;
        for (Auto i : autos) {
            if (i.isUnfallwagen()) {
                double erlass = ((i.getPreis() / 100) * (b * 100));
                zaehler += i.getPreis() - erlass;
            } else {
                double erlass = ((i.getPreis() / 100) * (a * 100));
                zaehler += i.getPreis() - erlass;
            }
        }
        return Math.round(100.0 * zaehler) / 100.0;
    }
    /**
     * Errechnet den Erlös in Prozent wenn alle Autos als Unfallwagen betrachtet werden.
     * @param a double kein Unfallwagen
     * @param b double Unfallwagen
     * @return double Erlös
     */
    public double erloes_alle_frei_pr(double a, double b){
        double ursprungswert = erloes_inkl_nachlass(a, b);
        double neuerwert = erloes_inkl_nachlass(a, a);
        double c = (neuerwert - ursprungswert) / ursprungswert * 100;
        return Math.round(100.0 * c) / 100.0;
    }
    /**
     * Errechnet den Erlös wenn alle Autos als Unfallwagen betrachtet werden.
     * @param a double kein Unfallwagen
     * @param b double Unfallwagen
     * @return double Erlös
     */
    public double erloes_alle_frei_eu(double a, double b){   
        double ursprungswert = erloes_inkl_nachlass(a, b);
        double erloesinproz = erloes_inkl_nachlass(a, a);
        double abssteigineu = erloesinproz-ursprungswert;
        return Math.round(100.0 * abssteigineu) / 100.0;
    }
}
